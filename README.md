# freeswitch PBX
FreeSWITCH is used to build and develop PBX, as an open-source telephony software for real-time communication protocols.

## Minimal FreeSWITCH Configuration

This "vanilla" configuration that comes with FreeSWITCH has
been designed as a showcase of the configurability of the myriad of
features that FreeSWITCH comes with out of the box. While it is very
helpful in tinkering with FreeSWITCH, it has a lot of extraneous stuff
enabled/configured for use in a production system. This configuration
aims to take the reverse stance -- it attempts to be a starting point
for configuring a new system by "adding" required features (instead of
removing them as one would do if one starts with the default
configuration).

This folder also includes the corresponding `modules.conf` that lists
the modules that are required to get this configuration working.

### Test

TODO